export function getRoute(source, destination) {
  return dispatch => {
    var directionsService = new google.maps.DirectionsService();
    directionsService.route(
      {
        origin: source,
        destination,
        travelMode: google.maps.TravelMode["TRANSIT"],
        provideRouteAlternatives: true
      },
      (response, status) => {
        if (status == "OK") {
          dispatch({
            type: "UPDATE_POSSIBLE_ROUTES",
            data: response
          });
        } else {
          window.alert("Directions request failed due to " + status);
        }
      }
    );
  };
}

export function onRoutePicked(key) {
  return dispatch => {
    dispatch({
      type: "UPDATE_PICKED_ROUTE",
      data: key
    });
  };
}

export function onRouteBooked(data) {
  return dispatch => {
    dispatch({
      type: "BOOK_ROUTE",
      data: data
    });
  };
}
