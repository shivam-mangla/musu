import React from "react";
import { withRouter } from "react-router-dom";
import { Row, Col, Card } from "reactstrap";
import PossibleRoute from "./PossibleRoute";

class PossibleRoutes extends React.Component {
  onRoutePicked = i => {
    this.props.onRoutePicked(i);
    this.props.history.push("/picked_route");
  };

  render() {
    const routes = [];
    let start_address = "";
    let end_address = "";
    this.props.possibleRoutes.forEach((route, i) => {
      if (!route.fare) {
        route.fare = "Not Available";
      }
      routes.push(
        <PossibleRoute
          route={route}
          index={i}
          key={i}
          onRoutePicked={() => this.onRoutePicked(i)}
          onRoutePickedAndBooked={() => {
            this.props.onRoutePicked(i);
            this.props.history.push("/book_trip");
          }}
        />
      );
    });

    if (!routes.length) return <div>No routes available! Sorry for this!</div>;

    start_address = this.props.possibleRoutes[0]["legs"][0]["start_address"];
    end_address = this.props.possibleRoutes[0]["legs"][0]["end_address"];

    return (
      <Card className="possible-routes-wrapper">
        <div className="possible-routes-meta">
          <div className="eticket-body-source-container">
            <span className="eticket-body-source-label">Source</span>
            <span className="eticket-body-source">
              {"..." + start_address.substr(start_address.length - 40)}
            </span>
          </div>
          <div className="eticket-body-destination-container">
            <span className="eticket-body-destination-label">Destination</span>
            <span className="eticket-body-destination">
              {"..." + end_address.substr(end_address.length - 40)}
            </span>
          </div>
          <h5>Routes Available</h5>
        </div>
        <div className="possible-routes-container">{routes}</div>
      </Card>
    );
  }
}

export default withRouter(PossibleRoutes);
