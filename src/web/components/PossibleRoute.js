import React from "react";
import { Row, Col, Card, Alert, Button, CardBody, Collapse } from "reactstrap";
import { duration } from "moment";

/**
 * I wish we had flow.. no time to install it rn
type JourneyTime = {text: string, timezone: string, value: DateTime};
type Props = {
  fare: {currency: string, text: string, value: number},
  legs: [
    transit: string,
    arrival_time: JourneyTime,
    departure_time: JourneyTime,
    distance: {text: string, value: number},
    duration: {text: string, value: number},
  ],
  onRoutePicked: () => {}
};
*/

class PossibleRoute extends React.Component {
  // keeping it for now, just in case we need it again
  // UNUSED
  state = { collapse: false };
  toggle = () => {
    this.setState({ collapse: !this.state.collapse });
  };

  render() {
    const {
      fare: { text: fare },
      legs: [
        {
          duration: { text: duration },
          steps,
          departure_time: { text: departure_time },
          arrival_time: { text: arrival_time }
        }
      ]
    } = this.props.route;

    let modes = [];
    steps.forEach((s, index) => {
      let mode =
        s.travel_mode === "TRANSIT"
          ? s.transit.line.vehicle.name
          : s.travel_mode;
      let html_instruction = s.instructions;
      modes.push(
        <div className="route-step" key={index}>
          {html_instruction}
        </div>
      );
    });

    return (
      <Card className="route-wrapper" onClick={this.props.onRoutePicked}>
        <div className="route-title-container">
          <span className="route-time">
            <span>Timing</span>
            <span>{departure_time + " - " + arrival_time}</span>
          </span>
          <span className="route-fare">
            <span>Fare</span>
            <span>{fare}</span>
          </span>
          <span className="route-duration">
            <span>Duration</span>
            <span>{duration}</span>
          </span>
        </div>
        <div className="route-modes">
          <div className="route-mode-heading">Route</div>
          <div className="route-mode-container">{modes}</div>
        </div>
        <Button className="route-book-button" block>
          View Details
        </Button>
        <Button
          className="route-book-button"
          block
          onClick={event => {
            this.props.onRoutePickedAndBooked();
            event.stopPropagation();
          }}
        >
          Book
        </Button>
      </Card>
    );
  }
}

export default PossibleRoute;
