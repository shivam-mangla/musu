import React from "react";
import PropTypes from "prop-types";
import { Row, Col, Card } from "reactstrap";
import Barcode from "./Barcode";

class Receipt extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    source: PropTypes.string.isRequired,
    destination: PropTypes.string.isRequired,
    barcode: PropTypes.string.isRequired,
    maproutes: PropTypes.any
  };

  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      date: props.date,
      source: props.source,
      destination: props.destination,
      barcode: props.barcode,
      route: props.maproutes.pickedRoute
    };
  }

  render() {
    const { id, date, barcode, route } = this.state;
    const {
      fare: { text: fare },
      legs: [
        {
          duration: { text: duration },
          end_address: end_address,
          start_address: start_address,
          departure_time: { text: departure_time },
          arrival_time: { text: arrival_time }
        }
      ]
    } = route;

    return (
      <div>
        <Row>
          <Col xs="12" className="eticket-container">
            <Card className="eticket-card-container">
              <div className="eticket-heading-container">
                <h3 className="eticket-heading">eTicket</h3>
              </div>
              <div className="eticket-body">
                <div className="eticket-body-meta">
                  <div className="eticket-id-container">
                    <span className="eticket-id-label">Passenger ID</span>
                    <span className="eticket-id"> {id}</span>
                  </div>
                  <div className="eticket-date-container">
                    <span className="eticket-date-label">Date</span>
                    <span className="eticket-date"> {date}</span>
                  </div>
                  <div className="eticket-date-container">
                    <span className="eticket-date-label">Duration</span>
                    <span className="eticket-date"> {duration}</span>
                  </div>
                  <div className="eticket-date-container">
                    <span className="eticket-date-label">Fare</span>
                    <span className="eticket-date"> {fare}</span>
                  </div>
                </div>
                <hr />
                <div className="eticket-body-main">
                  <div className="eticket-body-source-container">
                    <span className="eticket-body-source-label">Source</span>
                    <span className="eticket-body-source">
                      {"..." + start_address.substr(start_address.length - 40)}
                    </span>
                  </div>
                  <div className="eticket-body-source-container">
                    <span className="eticket-body-source-label">
                      Departure Time
                    </span>
                    <span className="eticket-body-source">
                      {departure_time}
                    </span>
                  </div>
                  <div className="eticket-body-destination-container">
                    <span className="eticket-body-destination-label">
                      Destination
                    </span>
                    <span className="eticket-body-destination">
                      {"..." + end_address.substr(end_address.length - 40)}
                    </span>
                  </div>
                  <div className="eticket-body-destination-container">
                    <span className="eticket-body-destination-label">
                      Arrival Time
                    </span>
                    <span className="eticket-body-destination">
                      {arrival_time}
                    </span>
                  </div>
                  <div className="eticket-body-barcode-container">
                    <Barcode code={barcode} />
                    <div className="eticket-body-barcode-message">
                      <span>
                        Scan the barcode to validate your ticket in DTC buses.
                      </span>
                      <span>
                        SMS 1453 with your Passenger ID to get confirmation code
                        in case of no internet.
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="eticket-footer">
                <span className="eticket-footer-message">© Musu</span>
              </div>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Receipt;
