import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import HomeMapComponent from "./HomeMap";
import { Row, Col, Card, Alert, Button, CardBody } from "reactstrap";

class PickedRouteStep extends React.Component {
  render() {
    const {
      duration: { text: duration },
      instructions,
      transit,
      travel_mode
    } = this.props.step;

    let icon = travel_mode;
    let button = <div />;
    if (travel_mode === "TRANSIT") {
      icon = transit.line.vehicle.name;
      button = (
        <Button
          className="route-book-button"
          block
          onClick={() => {
            this.props.history.push("/receipt");
          }}
        >
          Show E-ticket
        </Button>
      );
    }

    return (
      <Card className="route-wrapper">
        <div className="route-container">
          <Row>
            <Col>
              <HomeMapComponent
                onLocationChanged={() => {}}
                onMapMounted={this.onMapMounted}
              />
            </Col>
          </Row>
          <Col>{icon}</Col>
          <Col>{instructions}</Col>
          <div className="route-time">{duration}</div>
          <div className="route-modes">{travel_mode}</div>
          {button}
        </div>
      </Card>
    );
  }

  // Shouldn't use this.props here, I think it'll throw occasionally
  // because component can get unmounted
  onMapMounted = map => {
    var directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);
    directionsDisplay.setDirections(this.props.directionRendererReq);
  };
}

const mapStateToProps = ({ maproutes }) => ({
  directionRendererReq: maproutes.directionRendererReq,
  pickedRoute: maproutes.pickedRoute
});

export default connect(
  mapStateToProps,
  {}
)(withRouter(PickedRouteStep));
