import React from "react";
import { withRouter } from "react-router-dom";
import { Row, Col, Card, Alert, Button, CardBody } from "reactstrap";
import PickedRouteStep from "./PickedRouteStep";
import SwipeableViews from "react-swipeable-views";

/**
 * I wish we had flow.. no time to install it rn
type JourneyTime = {text: string, timezone: string, value: DateTime};
type Props = {
  fare: {currency: string, text: string, value: number},
  legs: [
    transit: string,
    arrival_time: JourneyTime,
    departure_time: JourneyTime,
    distance: {text: string, value: number},
    duration: {text: string, value: number},
  ],
  onRoutePicked: () => {}
};
*/

class PickedRoute extends React.Component {
  render() {
    const {
      legs: [
        {
          steps,
          arrival_time: { text: arrival_time }
        }
      ]
    } = this.props.pickedRoute;

    const prsteps = [];
    steps.forEach((step, it) => {
      prsteps.push(<PickedRouteStep key={it} step={step} />);
    });

    return (
      <div>
        <SwipeableViews
          enableMouseEvents={true}
          onChangeIndex={(index, indexLatest) => {
            console.warn("Index changed: ", index, indexLatest);
          }}
        >
          {prsteps}
        </SwipeableViews>
        {this.renderBookButton()}
      </div>
    );
  }

  renderBookButton() {
    return !this.props.isRouteBooked ? (
      <Button
        className="route-book-button"
        block
        onClick={() => this.props.history.push("/book_trip")}
      >
        Book
      </Button>
    ) : null;
  }
}

export default withRouter(PickedRoute);
