import React from "react";
import PropTypes from "prop-types";
import { geolocated, geoPropTypes } from "react-geolocated";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import config from "../../constants/config";

let currentLocation = geolocated({
  positionOptions: {
    enableHighAccuracy: true
  },
  userDecisionTimeout: 10000
});

export class MapContainer extends React.Component {
  state = { lat: 28.6087, lng: 77.01409 };

  centerMoved = (mapProps, map) => {
    this.setState({
      lat: map.center.lat(),
      lng: map.center.lng()
    });
    this.props.onLocationChanged(map.center.lat(), map.center.lng());
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.coords === null && nextProps.coords !== null) {
      const lat = nextProps.coords.latitude;
      const lng = nextProps.coords.longitude;
      this.setState({ lat, lng });
      const { onLocationChanged, onMapMounted } = this.props;
      onLocationChanged(lat, lng);
      if (onMapMounted) {
        onMapMounted(this.refs.map.map); // will only work because component will be mounted by then
      }
    }
  }

  render() {
    const { lat, lng } = this.state;
    return (
      <Map
        ref="map"
        google={this.props.google}
        zoom={14}
        className="home-map"
        onDragend={this.centerMoved}
        center={{ lat: lat, lng: lng }}
      >
        <Marker name={"Current location"} position={{ lat: lat, lng: lng }} />
      </Map>
    );
  }
}

MapContainer.propTypes = {
  google: PropTypes.object.isRequired
};

MapContainer.propTypes = Object.assign(
  {},
  MapContainer.propTypes,
  geoPropTypes
);

export default GoogleApiWrapper({
  apiKey: config.googleMapsApiKey
})(currentLocation(MapContainer));
