import React from "react";
import PropTypes from "prop-types";

class Barcode extends React.Component {
  static propTypes = {
    code: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    window.setTimeout(function() {
      JsBarcode("#barcode", props.code, {
        format: "CODE39",
        displayValue: true,
        width: 1
      });
    }, 0);
  }

  render() {
    return <svg id="barcode" />;
  }
}

export default Barcode;
