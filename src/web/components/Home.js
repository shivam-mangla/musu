import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  Card,
  Form,
  Alert,
  Input,
  Button,
  CardBody,
  FormGroup,
  CardHeader
} from "reactstrap";
import HomeMapComponent from "./HomeMap";
import { getRoute } from "../../actions/maproutes";

class Home extends React.Component {
  static propTypes = {
    source: PropTypes.object,
    destination: PropTypes.object,
    error: PropTypes.string,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    })
  };

  static defaultProps = {
    error: null,
    source: {},
    destination: {}
  };

  state = {
    source: this.props.source,
    destination: this.props.destination
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    const { error } = this.props;
    const { source, destination } = this.state;

    return (
      <div>
        <Row>
          <Col lg={{ size: 12 }} className="route-container">
            <Card>
              <CardBody>
                {!!error && <Alert color="danger">{error}</Alert>}
                <Form onSubmit={this.handleSubmit} className="route-form">
                  <Row>
                    <Col lg={{ size: 6 }}>
                      <Input
                        style={{ marginBottom: 10 }}
                        type="text"
                        name="source"
                        ref="source"
                        placeholder="Enter your source"
                        value={String(source.lat) + " , " + String(source.lng)}
                        onChange={this.handleChange}
                      />
                    </Col>
                    <Col lg={{ size: 6 }}>
                      <Input
                        style={{ marginBottom: 10 }}
                        type="text"
                        name="destination"
                        ref="destination"
                        placeholder="Enter your destination"
                        value={String(destination.lat) + " , " + String(destination.lng)}
                        onChange={this.handleChange}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Button
                        color="primary"
                        block
                        onClick={(event) => {
                          event.preventDefault();
                          this.setDestination(28.658719, 77.230335);
                        }}
                      >
                        Home
                      </Button>
                    </Col>
                    <Col>
                      <Button
                        color="primary"
                        block
                        onClick={(event) => {
                          event.preventDefault();
                          this.setDestination(28.5534, 77.1942);
                        }}
                      >
                        Work
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col lg={{ size: 12 }} className="route-container">
            <HomeMapComponent onLocationChanged={this.onLocationChanged} />
          </Col>
        </Row>
      </div>
    );
  }

  componentDidMount() {
    const { history, location } = this.props;
    history.replace({ ...location });
    this.setDestination = this.setDestination.bind(this);
  }

  setDestination = (lat, lng) => {
    this.setState({
      destination: { lat, lng }
    });
    this.props.fetchRoute(this.state.source, {lat, lng});
    // this.props.fetchRoute({lat: 28.6087, lng: 77.01409}, {lat, lng});
    this.props.history.push("/possible_routes");
  };

  onLocationChanged = (lat, lng) => {
    this.setState({
      source: { lat, lng }
    });
  };
}

const mapDispatchToProps = {
  fetchRoute: getRoute
};

export default connect(
  state => ({}),
  mapDispatchToProps
)(withRouter(Home));
