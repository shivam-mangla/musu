import React from "react";
import { Switch, Route } from "react-router-dom";

// Templates
import TemplateNothing from "../components/TemplateNothing";
import TemplateSidebar from "../components/TemplateSidebar";

// Routes
import Home from "../components/Home";

import RecipesContainer from "../../containers/Recipes";
import RecipesComponent from "../components/Recipes";
import RecipeViewComponent from "../components/Recipe";

import SignUpContainer from "../../containers/SignUp";
import SignUpComponent from "../components/SignUp";

import PickedRouteContainer from "../../containers/PickedRoute";
import PickedRouteComponent from "../components/PickedRoute";

import PaymentFormContainer from "../../containers/PaymentForm";
import PaymentFormComponent from "../components/PaymentForm";

import PossibleRoutesContainer from "../../containers/PossibleRoutes";
import PossibleRoutesComponent from "../components/PossibleRoutes";

import LoginContainer from "../../containers/Login";
import LoginComponent from "../components/Login";

import ForgotPasswordContainer from "../../containers/ForgotPassword";
import ForgotPasswordComponent from "../components/ForgotPassword";

import UpdateProfileContainer from "../../containers/UpdateProfile";
import UpdateProfileComponent from "../components/UpdateProfile";

import ReceiptContainer from "../../containers/Receipt";
import ReceiptComponent from "../components/Receipt";

import Error from "../components/Error";

const Index = () => (
  <Switch>
    <Route
      exact
      path="/"
      render={props => (
        <TemplateSidebar>
          <Home {...props} />
        </TemplateSidebar>
      )}
    />
    <Route
      path="/sign-up"
      render={props => (
        <TemplateNothing>
          <SignUpContainer {...props} Layout={SignUpComponent} />
        </TemplateNothing>
      )}
    />
    <Route
      path="/login"
      render={props => (
        <TemplateNothing>
          <LoginContainer {...props} Layout={LoginComponent} />
        </TemplateNothing>
      )}
    />
    <Route
      path="/forgot-password"
      render={props => (
        <TemplateNothing>
          <ForgotPasswordContainer
            {...props}
            Layout={ForgotPasswordComponent}
          />
        </TemplateNothing>
      )}
    />
    <Route
      path="/update-profile"
      render={props => (
        <TemplateSidebar>
          <UpdateProfileContainer {...props} Layout={UpdateProfileComponent} />
        </TemplateSidebar>
      )}
    />
    <Route
      path="/receipt"
      render={props => (
        <TemplateSidebar>
          <ReceiptContainer {...props} Layout={ReceiptComponent} />
        </TemplateSidebar>
      )}
    />
    <Route
      path="/recipes"
      render={props => (
        <TemplateSidebar>
          <RecipesContainer {...props} Layout={RecipesComponent} />
        </TemplateSidebar>
      )}
    />
    <Route
      path="/recipe/:id"
      render={props => (
        <TemplateSidebar>
          <RecipesContainer {...props} Layout={RecipeViewComponent} />
        </TemplateSidebar>
      )}
    />
    <Route
      path="/book_trip"
      render={props => (
        <TemplateSidebar>
          <PaymentFormContainer {...props} Layout={PaymentFormComponent} />
        </TemplateSidebar>
      )}
    />
    <Route
      path="/picked_route"
      render={props => (
        <TemplateSidebar>
          <PickedRouteContainer {...props} Layout={PickedRouteComponent} />
        </TemplateSidebar>
      )}
    />
    <Route
      path="/possible_routes"
      render={props => (
        <TemplateSidebar>
          <PossibleRoutesContainer
            {...props}
            Layout={PossibleRoutesComponent}
          />
        </TemplateSidebar>
      )}
    />
    <Route
      render={props => (
        <TemplateSidebar>
          <Error
            {...props}
            title="404"
            content="Sorry, the route you requested does not exist"
          />
        </TemplateSidebar>
      )}
    />
  </Switch>
);

export default Index;
