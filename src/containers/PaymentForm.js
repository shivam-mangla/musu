import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { onRouteBooked } from "../actions/maproutes";

const PaymentForm = ({
  Layout,
  onRouteBooked,
  maproutes: { pickedRoute },
  isLoading,
  infoMessage,
  errorMessage,
  successMessage
}) => (
  <Layout
    pickedRoute={pickedRoute}
    onRouteBooked={onRouteBooked}
    loading={isLoading}
    info={infoMessage}
    error={errorMessage}
    success={successMessage}
  />
);

PaymentForm.propTypes = {
  Layout: PropTypes.func.isRequired,
  maproutes: PropTypes.any,
  onRouteBooked: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  infoMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  successMessage: PropTypes.string
};

PaymentForm.defaultProps = {
  infoMessage: null,
  errorMessage: null,
  successMessage: null
};

const mapStateToProps = state => ({
  maproutes: state.maproutes || {},
  isLoading: state.status.loading || false,
  infoMessage: state.status.info || null,
  errorMessage: state.status.error || null,
  successMessage: state.status.success || null
});

const mapDispatchToProps = {
  onRouteBooked: onRouteBooked
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentForm);
