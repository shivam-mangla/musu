import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { onRoutePicked } from "../actions/maproutes";

const PossibleRoutes = ({
  Layout,
  onRoutePicked,
  maproutes,
  isLoading,
  infoMessage,
  errorMessage,
  successMessage
}) => (
  <Layout
    possibleRoutes={maproutes.possibleRoutes}
    loading={isLoading}
    info={infoMessage}
    error={errorMessage}
    success={successMessage}
    onRoutePicked={onRoutePicked}
  />
);

PossibleRoutes.propTypes = {
  Layout: PropTypes.func.isRequired,
  maproutes: PropTypes.any,
  onRoutePicked: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  infoMessage: PropTypes.string,
  errorMessage: PropTypes.string,
  successMessage: PropTypes.string
};

PossibleRoutes.defaultProps = {
  infoMessage: null,
  errorMessage: null,
  successMessage: null
};

const mapStateToProps = state => ({
  maproutes: state.maproutes || {},
  isLoading: state.status.loading || false,
  infoMessage: state.status.info || null,
  errorMessage: state.status.error || null,
  successMessage: state.status.success || null
});

const mapDispatchToProps = {
  onRoutePicked: onRoutePicked
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PossibleRoutes);
