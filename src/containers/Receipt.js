import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class Receipt extends React.Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    id: PropTypes.string,
    date: PropTypes.string,
    source: PropTypes.string,
    destination: PropTypes.string,
    fare: PropTypes.string,
    barcode: PropTypes.string,
    maproutes: PropTypes.any
  };

  static defaultProps = {
    id: "NDLS5482",
    date: "13 March 2019",
    source: "Rohini, New Delhi",
    destination: "Marathalli, Bangalore",
    fare: "1242.50",
    barcode: "ABCXS32128432",
    maproutes: {}
  };

  componentDidMount = () => {};

  render = () => {
    const { Layout, ...props } = this.props;
    props.date = new Date().toLocaleDateString();
    props.id = "NDLS" + (Math.floor(Math.random() * (9999 - 1000)) + 1000);
    return <Layout {...props} />;
  };
}

const mapStateToProps = state => ({
  maproutes: state.maproutes || {}
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Receipt);
