import Store from "../store/maproutes";

export const initialState = Store;

export default function mapRoutesReducer(state = initialState, action) {
  switch (action.type) {
    case "UPDATE_POSSIBLE_ROUTES": {
      if (action.data) {
        return {
          ...state,
          directionRendererReq: action.data,
          possibleRoutes: action.data.routes,
          pickedRoute: {},
          isRouteBooked: false
        };
      }
      return initialState;
    }
    case "UPDATE_PICKED_ROUTE": {
      return {
        ...state,
        pickedRoute: state.possibleRoutes[action.data]
      };
    }
    case "BOOK_ROUTE": {
      return {
        ...state,
        isRouteBooked: action.data
      };
    }
    default:
      return state;
  }
}
