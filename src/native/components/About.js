import React from "react";
import {
  Container,
  Content,
  Text,
  H1,
  H2,
  H3,
  Form,
  Item,
  Label,
  Input
} from "native-base";
import Spacer from "./Spacer";

const About = () => (
  <Container>
    <Content padder>
      <H1>Heading 1</H1>
      <Spacer size={10} />
      <Text>
        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus
        ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa
        justo sit amet risus. Etiam porta sem malesuada magna mollis euismod.
        Donec sed odio dui.
      </Text>
    </Content>
  </Container>
);

export default About;
