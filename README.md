## 🚀 Getting Started

#### 1. Clone and Install

_\*It's recommended that you install [React Native Debugger](https://github.com/jhen0409/react-native-debugger/releases) and open before `npm start`._

```bash
# Clone the repo
git clone https://github.com/mcnamee/react-native-starter-kit.git

# Install dependencies
yarn
cd server && npm install
```

#### 1.1. Run the _Web_ App

```bash
# Starts are local live-reload server at:
# http://localhost:3001
npm run web
```

Via webpack, starts a localhost server on port 3001 [http://localhost:3001](http://localhost:3001).

- Save code and it auto refreshes
- Install [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en) into Chrome to see the state of Redux

#### 1.2. Run express server

cd server/ && node server.js

# Google API pricing

We're using Directions, Places and maps. To prevent some mishap and random usage, I'll just not put the key in here and you can just add it to your local setup for testing.

- Quick sheet: https://cloud.google.com/maps-platform/pricing/sheet/
- More details: https://developers.google.com/maps/billing/understanding-cost-of-use

# Resources:

- Try this for google directions (service and renderer) API example: https://jsfiddle.net/qvc0fu2r/12/
- Flex in react-strap (read in an issue on the repo that this should work): http://getbootstrap.com/docs/4.1/utilities/flex/#align-items
- Response from Direction API (useful for our case: routes -> legs -> steps -> transit): https://developers.google.com/maps/documentation/directions/intro#Steps
- Redux if you forget: https://www.sohamkamani.com/blog/2017/03/31/react-redux-connect-explained/
- Reactstrap components (UI Library in this repo): https://reactstrap.github.io/components/layout/
- Sign In With Google: https://firebase.google.com/docs/auth/web/google-signin
- Uber API: https://developer.uber.com/docs/riders/references/api/v1.2/products-get

# Deployment

- npm run web-bundle
- firebase deploy

# Credits

Bootstrapped using awesome React Native Starter Kit - https://github.com/mcnamee/react-native-starter-kit
