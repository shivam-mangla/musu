const Uber = require('node-uber');

const uberConfig = {
  client_id: 'qO7Iidq9wV-1o3IhS1s8Z58HiKctkAEb',
  client_secret: '8nmVFSOsNMNtiKUse-SqXnOKTvzezOllu1k5ymoB',
  server_token: 'HNaSdgBg9vc3xZrrxGyRlIEbvutBweM-77kOqfAZ',
  redirect_uri: 'http://localhost:8081',
  name: 'musu uber',
  language: 'en_US',
  sandbox: true,
};

const getPriceEstimate = async (source, destination) => {
  // create new Uber instance
  const uber = new Uber(uberConfig);
  try {
    return await uber.estimates.getPriceForRouteByAddressAsync(source, destination);
  } catch (err) {
    return {
      error: err,
    };
  }
};

module.exports = {
  getPriceEstimate,
};
