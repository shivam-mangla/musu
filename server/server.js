const express = require('express');
const path = require('path');
const uber = require('./uber.js');

const app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.get('/ping', (req, res) => res.send('pong'));

app.get('/uber/estimate/price', (req, res) => {
  const { src, dest } = req.query;
  uber.getPriceEstimate(src, dest).then(r => res.send(r));
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(process.env.PORT || 5000);
